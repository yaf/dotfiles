- Rappel Vaccins
- rendez-vous podologue, renouvellement semelle
- rdv ophtalmo, voulais que je fasse une echo de l'œil, je retrouve pas l'ordonnace
- refaire le site d'ecd-sartrouville - extraire les header et footer
- refaire le site d'ecd-sartrouville - refaire les articles de blogs et vérifier que tout est là
- refaire le site d'ecd-sartrouville - expliquer à Sarah comment faire, et faire des essais avec elle
- revoir le site elsif.fr avec [Panash](https://framagit.org/yaf/panash) pour une mise en situation

- travailler sur funky.framadate
- analyser le sol au jardin http://fablabo.net/wiki/Analyse_du_sol_%C3%A0_faire_soi_m%C3%AAme
- Acheter des bocaux pour faire de la fermentation de legumes
- Transformer les questions de programmation en kata ?
- Couverture de test -> deploiement en prod en continue pour Merci Edgar
- Ajouter les utilisateurs et utilisatrices de Merci Edgar sur la mailling list
- Retranscrire les écrits sur la Ruche de Faure https://gallica.bnf.fr/ark:/12148/bpt6k67585g/f3.image.r=.langEN
- Poser des bases de projet sur https://glitch.com/ à destination des débutants
- Refaire ce site en html/css/JavaScript vanillia http://elements.wlonk.com/ElementsTable.htm voir http://elements.wlonk.com/ElementsTable.htm
- Explorer Racket http://docs.racket-lang.org/quick/index.html
- Faire un navigateur http+markdown (Gopher ?)
- Faire un exercice de fonction calisthenics https://codurance.com/2017/10/12/functional-calisthenics/
- Faire un exercice d'objet calisthenics https://williamdurand.fr/2013/06/03/object-calisthenics/
- http://greaterthanorequalto.net/ Inspiration
- Refaire le site d'ouverture facile http://www.ouverture-facile.com/
- renouveller la carte identité, elle est prolongé, et **valable jusqu'en 2023**.
- Faire l'exercice de fabriquer une base de donnée https://cstack.github.io/db_tutorial/
- Lire Tolstoï, Paul Robin, Francisco Ferrer ?
- Sébastien Faure, Écrits Pédagogiques , Éd. du Monde Libertaire, réédition 1992
- https://www.ageca.org/
- https://lobste.rs/
- https://www.ekopedia.fr/wiki/Accueil
- Roland Lewin, Sébastien Faure et « La Ruche » , ou l'Éducation Libertaire , Éd. Ivan Davy, 1989
- Édouard Stéphan, La Ruche, une école libertaire au Pâtis à Rambouillet, 1905-1917 , Éd. SHARY, 2000
- Sujet d'exercice ? https://github.com/mame/quine-relay
- Rencontré aux RMLL 2018, un lyonnais : Mougeot http://www.cedrats.org/
- Fabriquer un Cajon https://fr.wikihow.com/fabriquer-un-caj%C3%B3n
- rejoindre deepgreenresistance ? https://deepgreenresistance.fr/rejoindre-deep-green-resistance/
- renouvellement medoc - **rendez-vous toubib à reprendre**
- https://github.com/gfredericks/quinedb
- https://github.com/arnauddri/algorithms
- Kata manquant ? https://github.com/sandromancuso/trip-service-kata
- Elm ? http://elm-lang.org/
- Faire du pain
- Savoir filter de l'eau
- ? Stoker des médicaments
- Acheter un vélo d'occasion
- Passer son psc1
- Créer un fond d'urgence en cash
- Laisser sa carte bleue chez soi
- Fabriquer du beurre
- Savoir reconnaître 10 plantes utiles
- Apprendre l'anatomie humaine
- Clef usb avec documents importants
- Étudier Orlov
- Devenir travailleur indépendant
- Lire Bill Mollison
- Fabriquer sa lessive

## Lunette - Lecture de code

* Dataflow analysis  https://en.m.wikipedia.org/wiki/Data-flow_analysis
* Lecture de code suivi
* Un page web pour parler du concept
* Documenter les formats
* Faire de la poésie sourcecodepoetry.com
* Repérer les code Smell
* Passer sur Sparrow Decks
* Sparrow deck sur les plantes comestibles, sur les arbres, sur les insectes, sur les vers de terre, sur les fleurs, ...

## Jardin Robinson

* https://ushare-docs.frama.wiki/
* https://wiki-robinson.frama.wiki/
* Ajouter et organiser la documentation dans le wiki
* Simple jolie page d'explications (pour complément wiki)
* Analyser le plomb dans le sol, il doit être < 400 ppm si le PH est >= 7 Peut-être faut-il tester le PH avant parce que c'est plus facile ?
* Une plante qui nettoie le sol des métaux lourds http://www.tela-botanica.org/bdtfx-nn-103108-synthese peut-être aussi http://www.humanite-biodiversite.fr/article/des-plantes-hyperaccumulatrices-de-metaux-lourds, la moutarde aussi le fait https://fr.wikipedia.org/wiki/Moutarde_brune
* le fablabo fait des analyses de sol http://fablabo.net/wiki/Analyse_du_pH_du_sol
* Lister les vidéos et autres inspirations intéressante, commencer parle jardin du paresseux https://www.youtube.com/channel/UCm-SeCr6-0dPzfUT8gZvYLg
* Plantes indicatrices de polution http://fablabo.net/wiki/Plantes_bio-indicatrices voir aussi http://permaforet.blogspot.fr/2013/04/plantes-bio-indicatrices.html https://sites.google.com/site/tpesurlaphytoremediationgroupe/La-depollution-des-metaux-lourds


## Livres

* [Penser par sois même](http://www.eclm.fr/ouvrage-383.html)
* [Sentir-penser avec la Terre, Arturo Escobar]()
* [Sébastien Faure Et "La Ruche" Ou L'éducation Libertaire - Roland Lewin](https://www.priceminister.com/offer/buy/577123/Lewin-Sebastien-Faure-Et-Ruche-Livre.html)
* [Vivre ma vie. Une anarchiste au temps des révolutions](https://www.lechappee.org/agenda/vivre-ma-vie-une-anarchiste-au-temps-des-revolutions-7)
* [Sentir-penser avec la Terre](http://www.seuil.com/ouvrage/sentir-penser-avec-la-terre-arturo-escobar/9782021389852)
* [Surveiller et punir](https://fr.wikipedia.org/wiki/Surveiller_et_punir) de Michel Foucault
* [Le Changement climatique expliqué à ma fille](http://www.seuil.com/ouvrage/le-changement-climatique-explique-a-ma-fille-jean-marc-jancovici/9782020965972)
* [Le capitalisme expliqué à ma petite fille en espérant qu'elle en voie la fin](http://www.seuil.com/ouvrage/le-capitalisme-explique-a-ma-petite-fille-jean-ziegler/9782021397222)
* [Livres enfants éducation](https://www.auxeditionsduphare.com/)
* [Économie Symbiotique](https://www.actes-sud.fr/catalogue/economie/leconomie-symbiotique)
* [Magasin Général](https://regisloisel.com/magasin-general)
* [reclaim](https://www.cambourakis.com/spip.php?article786)
* [Femmes, magie et politique Starhawk](http://www.peripheries.net/article215.html)
* [Manuel de transition](https://www.amazon.fr/Manuel-transition-Rob-Hopkins/dp/2923165667/ref=sr_1_9?ie=UTF8&qid=1526024372&sr=8-9&keywords=pablo+servigne)
* [Petit traité de résilience locale](https://www.amazon.fr/Petit-trait%C3%A9-r%C3%A9silience-locale-Agn%C3%A8s/dp/2843771862/ref=sr_1_6?ie=UTF8&qid=1526024372&sr=8-6&keywords=pablo+servigne&dpID=51bG2xiaqOL&preST=_SY291_BO1,204,203,200_QL40_&dpSrc=srch)
* [L'évaluation du travail à l'épreuse du réel](https://www.cairn.info/l-evaluation-du-travail-a-l-epreuve-du-reel--9782759224609.htm)
* [Stratégies végétales, petits arrangements et grandes manoeuvres Relié – 15 décembre 2011 de Benoît Garrone (Auteur),‎ Philippe Martin (Auteur),‎ Bertrand Schatz (Auteur),‎ Les Ecologistes de l'Euzière (Auteur)](https://www.amazon.fr/Strat%C3%A9gies-v%C3%A9g%C3%A9tales-arrangements-grandes-manoeuvres/dp/2906128287?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&tag=duc-21&linkCode=xm2&camp=2025&creative=165953&creativeASIN=2906128287)
* [Change the World Without Taking Power](https://en.wikipedia.org/wiki/Change_the_World_Without_Taking_Power)
* [Libres savoirs : les biens communs de la connaissance](https://tempsdescommuns.org/libres-savoirs/)
* http://www.piecesetmaindoeuvre.com/spip.php?page=resume&id_article=960
* [Stratégies végétales, petits arrangements et grandes manoeuvres](https://www.amazon.fr/Strat%C3%A9gies-v%C3%A9g%C3%A9tales-arrangements-grandes-manoeuvres/dp/2906128287)
* [La symbiose](https://www.amazon.fr/symbiose-Marc-Andr%C3%A9-Selosse/dp/2711752836)
* [Éloge de la plante Francis Hallé](http://www.seuil.com/ouvrage/eloge-de-la-plante-pour-une-nouvelle-biologie-francis-halle/9782020684989)
* [Un monde sans hiver Francis Hallé](http://www.seuil.com/ouvrage/un-monde-sans-hiver-francis-halle/9782757838242)
* [Éducation libertaire par l'auteur Hugues Lenoir](http://www.hugueslenoir.fr/presentation/)
* [Du bon usage de la pédagogie](https://www.icem-pedagogie-freinet.org/node/50934)
* [Faire des hommes libre, boimondeau](http://editionsrepas.free.fr/editions-repas-livre-boimondau.html)
* [Dormez tranquille jusqu'en 2100](https://www.odilejacob.fr/catalogue/sciences/environnement-developpement-durable/dormez-tranquilles-jusquen-2100_9782738132529.php)
* [La permaculture de Sepp Holzer](https://librairie-permaculturelle.fr/jardin-foret/2-livre-la-permaculture-de-sepp-holzer.html)
* [Fernand Pelloutier et les Origines du syndicalisme d'action directe](http://www.seuil.com/ouvrage/fernand-pelloutier-et-les-origines-du-syndicalisme-d-action-directe-jacques-julliard/9782020026710)
* [Voyage en misarchie](http://editionsdudetour.com/index.php/les-livres/voyage-en-misarchie/)
* [L'agroécologie](https://librairie-permaculturelle.fr/agroecologie/48-livre-l-agroecologie-miguel-altieri.html)
* [Logique de l'action collective](https://www.amazon.fr/Logique-laction-collective-Mancur-Olson/dp/2800415029)
* [Commun village](http://editionsrepas.free.fr/editions-repas-livre-commun-village.html)
* [Introduction à la Permaculture](http://www.passerelleco.info/article.php?id_article=1708)
* [Une société sans école](https://www.amazon.fr/soci%C3%A9t%C3%A9-sans-%C3%A9cole-Ivan-Illich/dp/2757850083)
* [La fin de l'éducation ? Commencements... ](https://www.babelio.com/livres/Lepri-La-fin-de-leducation--Commencements/521383)
* [Permaculture : le guide pour bien débuter : Jardiner en imitant la nature](https://www.amazon.fr/Permaculture-d%C3%A9buter-Jardiner-imitant-nature/dp/2815306174)
* [La révolte luddite : Briseurs de machines à l'ère de l'industrialisation Kirkpatrick Sale]()
* [Le macrosope](https://fr.wikipedia.org/wiki/Le_Macroscope)


## Les brevets de Célestin Freinet

_issu de l'article http://svt-egalite.fr/index.php/outils/evaluer-avec-les-brevets-de-celestin-freinet_

Deux types de brevets :
- Obligatoires
  - Écrivain
  - Lecture
  - Écriture
  - Bon langage
  - Maître en orthographe
  - Historien
  - Géographie
  - Ingénieur en eau et liquides divers
  - Ingénieur de l'air et des gaz
  - Ingénieur des végétaux et des cultures
  - Ingénieur des minéraux divers
  - Ingénieur du fer
  - Calculateur
- Accessoires
  - Imprimeur
  - Enquêteur
  - Peintre
  - Marionnettiste
  - Nageur
  - Campeur
  - Potier
  - Cuisinier
  - Électricien
  - Bon camarade
  - Bon correspondant
  - Etc

Trois type de brevet :
- Tests. Vérifier des choses mesurables
- Composés. Ensembl de taches à mener à bien
- Chef-d'œuvre. Production libre de l'élèvre


## CodeSmell

code smell

Bloates
* long method
* large class
* data clamps
* long parameters list

Tool abusers
* switch statement
* refused bequest/request
* temporary fields

Change preventers
* Divergent changes
* Shotgun surgery
* parallel inheritance

dispensables
* lazy class
* speculative generality
* data class
* duplicate code

couplers
* feature envy
* inappropriate intinacy
* message chains
* middle man


## Livre Papert


<<<
Je ne crois pas que ce soit les ordinateurs eux même qu'il faille redouter, mais bien plutôt la façon dont la culture digérera la presence de l'ordinateur.
<<<
-- Seymour Paper, Jaillissement de l'esprti, 1980.


<<<
De même que dans un bon cours d'initiation artistique, l'enfant acquiert des connaissance tech comme un moyen de parvenir à un but créatif qu'il s'est lui même fixé.
<<<
-- Seymour Paper, Jaillissement de l'esprti, 1980.



Page 43

<<<
On se pose souvent la question de savoir si les enfants, à l'avenir, programmeront des ordinateurs, ou s'ils seront au contraire embrigadés dans des activités programmées pour eux à l'avance. La réponse est à coup sûr que pour certains enfants la première hypothèse sera la bonne, pour d'autres la seconde, pour d'autres encore les deux, et pour certains ni l'une ni l'autre. Mais quels enfants entreront dans quelle catégorie ? Et plus précisément, les enfants de quelle classe sociale ? La réponse dépendra du genre d'activités informatiques et d'environnement qui leur seront offerts.
<<<


page 44

<<<
Mais cette illustration (usage de l'ordinateur comme les adultes, pour faire du traitement de texte) de la manière dont l'ordinateur peut contribuer à la maitrîse du langage chez les enfants est en opposition flagrante avec la réalité
que l'on voit se mettre en place dans la plupart des écoles primaires. L'ordinateur est perçu comme une machine à enseigner. Cette machine permet de faire faire aux enfants différents exercice...

Cet écart de conception, à mon avis, dépasse très largement le cadre d'un simple choix technique entre deux stratégies d'enseignements. Il reflète en fait une opposition radicale entre deux philosophie de l'éducation.

L'usage de l'ordinateur pour écrire et rédiger offre aux enfants la possibilité de se comporter davantages en adultes tant à l'égard de leur propre travail intellectuel que vis-à-vis d'eux-mêmes. Ce faisant, il est donc inévitable qu'il heure de front l'école traditionelle dont l'effet, sinon l'intention, est bien souvent d'"infantiliser" l'enfant.
<<<


## Note du livre pour apprendre le C

3 types d'instruction :

* simple, terminé par un ; ou un . ou rien (fin de ligne ?)
* structurations (if, for...)
* des blocs ({})

Type de base

Char, int et float sont dit des scalaires, ou simples. Les variables ne contiennent qu'une valeur (où est le booléen ?)

//vs//

Les types structurées (dit aussi agrégés)

Les pointeurs et énumération sont considéré simple aussi...

Les tableaux sont des types dérivés (?)

On retrouve souvent les opérateurs dans les explications de langage...

* arithmetique : adition, soustraction, division, ....
* comparaisons, relationnel
* logiques
* affectation


//''les entrées sorties''//, voilà un axe nouveau et intéressant.

console, fichier, socket, ...


On parle ensuite des instructions de controle
On parle de choix et de boucle
Du coup, un lien avec les opérateurs logiques ? ...
On parle aussi de branchement conditionnel et inconditionnel `goto`, `break`, `continue`

La portée des variables et abordé dans les functions dans le livre sur le C


## Note - Sol suite mooc Fun et autre

! Notes à propos du phosphore

Issu du mooc  https://www.fun-mooc.fr/courses/course-v1:Agreenium+66001S03+session03/courseware/c1478e640cfc42c8b58ba8e189e48b57/e14b21a35289457999036c57a0c5a6e3/

Le phosphore (P) est un nutriment très important pour les plantes, et c’est souvent l’élément le plus limitant de la croissance végétale. Or c’est une ressource difficile d’accès dans le sol : soit parce qu’elle est peu présente (c’est le cas de sols tropicaux souvent très anciens, qui se sont appauvris en P au cours du temps), soit parce qu’elle est présente, mais sous des formes très peu disponibles pour les plantes ; soit pour les deux raisons à la fois.

Plantes en contenant assez bien :

Les plantes contenant le plus de phosphore sont la datura(Datura) et la jusquiame(Hyoscymus) qui poussent surtout dans les terrains qui n’en contiennent pas.

http://www.fleurs-des-champs.com/phototheque/fiche_plante.php?code=653

http://plantes.sauvages.free.fr/pages_plantes/jusquiame_noire_.htm

Légumes en contenant assez bien (valeurs données en mg/100 g):

* Courgette 7
* Epinard 15
* Chou fleur 20
* Oignon 23
* Pastèque 26
* Fraise 27
* Chou 36
* Brocoli 46
* Echalotte 50
* Asperge 50
* Tomate 63
* Pomme de terre 78
* Mais 79
* Artichaut 103
* Pois 187

_source http://naturalyss.over-blog.com/article-les-plantes-et-le-phosphore-55290721.html_

Lire aussi ? http://www.plantyfolia.com/dossier_pratique/besoin_nutritif_plante_engrais#/dossier_pratique/besoin_nutritif_plante_engrais

