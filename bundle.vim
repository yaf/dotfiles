" Required
Bundle 'gmarik/vundle'

" Plugins
Bundle 'vim-scripts/L9'
Bundle 'vim-ruby/vim-ruby'
Bundle 'pangloss/vim-javascript'
Bundle 'tpope/vim-markdown'
Bundle 'kchmck/vim-coffee-script'
Bundle 'brentmc79/vim-rspec'
Bundle 'scrooloose/nerdtree'
Bundle 'tpope/vim-git'
Bundle 'tpope/vim-haml'
Bundle 'tpope/vim-rake'
Bundle 'tpope/vim-rails'
Bundle 'tpope/vim-markdown'
Bundle 'tpope/vim-surround'
Bundle 'tpope/vim-bundler'
Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/vim-powerline'
Bundle 'cakebaker/scss-syntax.vim'
Bundle 'jnwhiteh/vim-golang'
Bundle 'oscarh/vimerl'
Bundle 'kien/ctrlp.vim'

" Colorschemes
Bundle 'moria'
Bundle 'desert256.vim'
Bundle 'jellybeans.vim'
Bundle 'altercation/vim-colors-solarized'
