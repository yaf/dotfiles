Installation
============

install with
`git clone https://github.com/yaf/dotfiles.git ~/.vim && .vim/setup`

or, clone the repo in a ~/.vim directory
`git clone https://github.com/yaf/dotfiles.git ~/.vim`

and then run the setup:
`~/.vim/setup`

Mostly inspired by JulienXX dotvim https://github.com/julienXX/dotvim

